-- ARTIST --

-- Add 15 new Artists to the Artist table. (ArtistId, Name)

INSERT INTO Artist (Name) VALUES ('Madsen');
INSERT INTO Artist (Name) VALUES ('The Last Sighs of the Wind');
INSERT INTO Artist (Name) VALUES ('Black Label Society');
INSERT INTO Artist (Name) VALUES ('Freddie Mercury');
INSERT INTO Artist (Name) VALUES ('Incubus');
INSERT INTO Artist (Name) VALUES ('Metallica');
INSERT INTO Artist (Name) VALUES ('Nightwish');
INSERT INTO Artist (Name) VALUES ('Black Veil Brides');
INSERT INTO Artist (Name) VALUES ('Parachute');
INSERT INTO Artist (Name) VALUES ('Queen');
INSERT INTO Artist (Name) VALUES ('Puddle Of Mudd');
INSERT INTO Artist (Name) VALUES ('Die Toten Hosen');
INSERT INTO Artist (Name) VALUES ('Theory of a Dead Man');
INSERT INTO Artist (Name) VALUES ('Black And Yellow');
INSERT INTO Artist (Name) VALUES ('Nirvana');


SELECT * FROM Artist WHERE ArtistId BETWEEN 1 AND 10 ORDER BY Name DESC;
SELECT * FROM Artist WHERE ArtistId BETWEEN 1 AND 5 ORDER BY Name;
SELECT * FROM Artist WHERE Name LIKE 'Black%'; 
SELECT * FROM Artist WHERE Name LIKE  '%Black%';


-- Employee --


SELECT FirstName, LastName FROM Employee WHERE City = 'Calgary';
SELECT FirstName, LastName, BirthDate, MIN(BirthDate) FROM Employee;
SELECT * FROM Employee WHERE ReportsTo LIKE 2;
SELECT EmployeeId FROM Employee WHERE FirstName = 'Nancy' AND LastName='Edwards';
SELECT COUNT(*), City FROM Employee GROUP BY City HAVING City = 'Lethbridge';

-- Invoive --

SELECT Count(*) FROM Invoice WHERE BillingCountry = 'USA';
SELECT Max(total) FROM Invoice;
SELECT Min(total) FROM Invoice;
SELECT COUNT(*) FROM Invoice WHERE Total < 5;
SELECT COUNT(InvoiceId) FROM Invoice WHERE BillingState IN ("CA", "TX", "AZ");
SELECT AVG(Total) FROM Invoice;
SELECT SUM(Total) FROM Invoice;
s