CREATE DATABASE MiriamGaGu_People;

# Schema

CREATE TABLE People(
    Id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    Name VARCHAR(50) NOT NULL,
    Age INT(3),
    Height INT(3),
    City VARCHAR(50),
    Favotite_color VARCHAR(50)

);

INSERT INTO People (Name, Age, Height, City,Favotite_color) VALUES ('Addy Osmani', 20, 178, 'Clifornia', 'yellow' );
INSERT INTO People (Name, Age, Height, City,Favotite_color) VALUES ('Paul Irish', 29, 191, 'Austin', 'orange');
INSERT INTO People (Name, Age, Height, City,Favotite_color) VALUES ('Eric Elliot', 30, 165, 'Portland', 'blue');
INSERT INTO People (Name, Age, Height, City,Favotite_color) VALUES ('Jack Wilshere', 18, 189, 'San Francisco', 'purple');
INSERT INTO People (Name, Age, Height, City,Favotite_color) VALUES ('Ayrton Senna', 33, 170, 'Texas', 'green');
INSERT INTO People (Name, Age, Height, City,Favotite_color) VALUES ('Marek Sedlak', 32, 187, 'Slovakia', 'blue' );
INSERT INTO People (Name, Age, Height, City,Favotite_color) VALUES ('Martin Truehan', 31, 178, 'Prague', 'gree' );
INSERT INTO People (Name, Age, Height, City,Favotite_color) VALUES ('Lubica Sedlakova', 58, 179, 'Slovakia', 'purple');
INSERT INTO People (Name, Age, Height, City,Favotite_color) VALUES ('Michael Robinson', 30, 189, 'Australia', 'red');
INSERT INTO People (Name, Age, Height, City,Favotite_color) VALUES ('Alexa Reid', 29, 175, 'Englad', 'pink');


SELECT * FROM People ORDER BY Height;
SELECT * FROM People ORDER BY Height DESC;
SELECT * FROM People ORDER BY Age DESC;
SELECT * FROM People ORDER BY Age  > 20;
SELECT * FROM People ORDER BY Age  = 18;
SELECT * FROM Person WHERE Age < 20 OR Age > 30;
SELECT * FROM People WHERE Age <> IS 27;
SELECT * FROM People WHERE Favotite_color <>'red';
SELECT * FROM Person WHERE NOT Favorite_color = "red" AND NOT Favorite_color = "blue";
SELECT * FROM Person WHERE Favorite_color = "orange" OR Favorite_color = "green";
SELECT * FROM Person WHERE Favorite_color IN ("orange", "green", "blue");
SELECT * FROM Person WHERE Favorite_color IN ("yellow", "purple");

 CREATE TABLE Orders(
     Id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
     PersonId INT NOT NULL,
     Product_Name VARCHAR(60),
     Num_of_orders INT NOT NULL,
     Product_price INT NOT NULL,
     FOREIGN KEY (PersonId) REFERENCES(Id)
     
     
 );

INSERT INTO Orders(PersonId, Product_Name, Num_of_orders, Product_price) VALUES (6, "Canon PowerShot SX420 IS", 1, 5599);
INSERT INTO Orders(PersonId, Product_Name, Num_of_orders, Product_price) VALUES (9, "Nikon Coolpix B500 ", 2, 6050);
INSERT INTO Orders(PersonId, Product_Name, Num_of_orders, Product_price) VALUES (7, "Nikon Cámara Reflex D5600", 1, 16999);
INSERT INTO Orders(PersonId, Product_Name, Num_of_orders, Product_price) VALUES (6, "Nikon Coolpix P900", 1, 19398);
INSERT INTO Orders(PersonId, Product_Name, Num_of_orders, Product_price) VALUES (7, "Sony DSC-H300", 1, 4999);


 SELECT * FROM Orders;
 SELECT SUM(Num_of_orders) FROM Orders;
 SELECT SUM(Num_of_orders * Product_price) FROM Orders;
 SELECT SUM(Num_of_orders * Product_price) FROM Orders WHERE PersonId = 6;



